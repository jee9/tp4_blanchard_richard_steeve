/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbds.bonbank.gestionnaires;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import com.mbds.bonbank.modeles.CompteBancaire;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;
import javax.persistence.Query;

/**
 *
 * @author Dell
 */
@Stateless
public class CompteBancaireBean {

    @PersistenceContext(unitName = "TP04_BLANCHARD_RICHARD_STEEVEPU")
    private EntityManager em;

    public void persist(Object object) {
        em.persist(object);
    }

    public CompteBancaire createAccount(String firstName, String lastName, double montant){
        CompteBancaire compteBancaire = new CompteBancaire();
        compteBancaire.setFirstName(firstName);
        compteBancaire.setLastName(lastName);
        compteBancaire.setBalance(montant);
        Random random = new Random();
        compteBancaire.setAccountNumber(firstName.charAt(0)+""+lastName.charAt(0)+"-"+String.format("%04d", random.nextInt(9999)));  
        em.persist(compteBancaire);
        return compteBancaire;
    }
    
    public Collection<CompteBancaire> listeAccount(){
        Query q = em.createQuery("select c from CompteBancaire c");
        return q.getResultList();
    }
    
    public CompteBancaire getCompte(int id) {  
        Query q = em.createQuery("select c from CompteBancaire c where id = :id ");
        q.setParameter("id", id);
        List<CompteBancaire> c = q.getResultList(); 
        if(c!=null && c.size()>0){
            return c.get(0);
        }else{
            return null;
        }
    }  
    
    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
}

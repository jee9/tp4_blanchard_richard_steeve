<%-- 
    Document   : newAccount
    Created on : May 6, 2021, 9:03:53 PM
    Author     : Dell
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Compiled and minified CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

        <title>Nouveau Compte</title>
    </head>
    <body>
        <jsp:include page="header.jsp" /> 
        
        <div class="container">
            
            <h4>Nouveau Compte</h4>
            
            <div class="card-panel">
            <div class="row">
   <form class="col s12" action="BankServlet" method="post">
      <div class="row">
        <div class="input-field col s6">
          <input placeholder="Placeholder" id="first_name" name="first_name" type="text" class="validate">
          <label for="first_name">First Name</label>
        </div>
        <div class="input-field col s6">
          <input id="last_name" name="last_name" type="text" class="validate">
          <label for="last_name">Last Name</label>
        </div>
      </div>
      
      <div class="row">
        <div class="input-field col s12">
            <input id="montant" name="montant" type="number" >
            <label for="email">Montant</label>
        </div>
      </div>
          <button class="btn waves-effect waves-light" type="submit" name="action">
                Valider
           </button>
    </form>
    </div>
                
               
        </div>
            
        </div>
        
        
        
        
          <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    </body>
</html>

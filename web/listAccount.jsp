<%-- 
    Document   : listAccount
    Created on : May 6, 2021, 9:04:26 PM
    Author     : Dell
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
    <head>
        
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        <!-- Compiled and minified CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

        <title>Liste Compte</title>
    </head>
    <body>
          <jsp:include page="header.jsp" /> 
          
         <div class="container">
            
            <h4>Liste Compte</h4>
            
            <div class="card-panel">
            <div class="row">
                
                  <table>
        <thead>
          <tr>
              <th>Numero Compte</th>
              <th>FirstName</th>
              <th>LastName</th>
              <th>Balance</th>
          </tr>
        </thead>

        <tbody>
                <c:forEach var="u" items="${listcb}">
                    <tr>
                        <td>${u.accountNumber}</td>
                        <td>${u.firstName}</td>
                        <td>${u.lastName}</td>
                        <td>${u.balance}</td>
                    </tr>
                </c:forEach>
        </tbody>
      </table>
    
    </div>
                
               
        </div>
            
        </div>
        
         <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    </body>
</html>
